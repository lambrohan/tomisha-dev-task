module.exports = {
  theme: {
    extend: {
      colors: {
        primary: '#319795',
        textDark: '#2D3748',
      },
    },
  },
}
